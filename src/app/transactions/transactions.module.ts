import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Transactions } from './transactions.component';

export const routes = [
  { path: '', component: Transactions, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Transactions
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export default class TransactionsModule {
  static routes = routes;
}

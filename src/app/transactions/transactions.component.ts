import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransactionService } from '../services/TransactionService';
import { MerchantdetailModel } from '../models/MerchantdetailModel';
@Component({
  selector: 'transactions',
  styleUrls: ['./transactions.style.scss'],
  templateUrl: './transactions.template.html',
  providers: [TransactionService],
  encapsulation: ViewEncapsulation.None,
})
export class Transactions {

  public merchantId: string;
  public errordiv: boolean;
  public merchantData: MerchantdetailModel;
  constructor(public _transactionService: TransactionService, public router: Router, ) {

    if (localStorage.getItem("merchantId") != null || localStorage.getItem("merchantId") != undefined) {
      this.merchantId = localStorage.getItem("merchantId");
      this.getMerchantById();
      localStorage.setItem("merchantId", "");
    }
    this._transactionService.getTransactionDetailsById(-1).subscribe(a => {

      this.merchantData = a.data;

      for (var i = 0; i < a.data.length; i++) {
        var d = new Date(+this.merchantData[i].transactionTime * 1000);
        this.merchantData[i].transactionTime = d;
      }
    })

  }

  getTransactionDetailsForThisMerchant(elem) {

    console.log(localStorage.getItem("TransactionId"));
    localStorage.setItem("TransactionId", elem);
    this.router.navigate(["/app/dashboard"]);

  }
  getMerchantById() {

    this.merchantId = jQuery.trim(this.merchantId);
    if (this.merchantId == undefined || this.merchantId.length == 0) {
      this.errordiv = true;
    } else {

      this.errordiv = false;

      this._transactionService.getTransactionDetailsById(this.merchantId).subscribe(a => {

        this.merchantData = a.data;

        for (var i = 0; i < a.data.length; i++) {
          var d = new Date(+this.merchantData[i].transactionTime * 1000);
          this.merchantData[i].transactionTime = d;
        }
      })


    }
  }

}
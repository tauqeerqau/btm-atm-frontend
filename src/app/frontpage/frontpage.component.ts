import { Component, ViewEncapsulation, Input, Renderer } from '@angular/core';
import { AppConfig } from '../app.config';
import { TransactionModel } from '../models/TransactionModel';
import { TransactionService } from '../services/TransactionService';
declare var jQuery: any;
@Component({
  selector: 'frontpage',
  templateUrl: './frontpage.template.html',
  styleUrls: ['./frontpage.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [TransactionService]
})
export class Frontpage {
  config: any;
  month: any;
  year: any;
  angular: any;
  public receivingProfit: any;
  public sendingProfit: any;
  public totalProfit: any;
  public receivingVolume: any;
  public sendingVolume: any;
  public totalVolume: any;
  public transaction: TransactionModel;
  public transactionId: string;
  public block_hash: string;
  public block_height: string;
  public showTransactionDetails: boolean = false;
  public errorDiv: boolean = false;
  public time: any;
  public block_time; confirmations; contains_dust; estimated_change; estimated_change_address; estimated_value; first_seen_at; hash; high_priority: string;
  public is_coinbase; is_double_spend; last_seen_at; lock_time_block_height; lock_time_timestamp; size; total_fee; total_input_value; total_output_value; unconfirmed_inputs: string;


  constructor(private renderer: Renderer, config: AppConfig, public TransactionService: TransactionService) {
    this.config = config.getConfig();


    this.TransactionService.getProfitStatisticsByTime(1).subscribe(a => {
      this.receivingProfit = a.data.receivingProfit;

      this.sendingProfit = a.data.sendingProfit;
      this.totalProfit = a.data.totalProfit;
      

    });

    this.TransactionService.getTransactionStatisticsByTimeRoute(this.time).subscribe(a => {
      console.log(a);
      this.receivingVolume = a.data.receivingVolume;
      this.sendingVolume = a.data.sendingVolume;
      this.totalVolume = a.data.totalVolume;
    })

  }

  getProfitTime(elem) {
    if (elem == "Daily") {
      this.TransactionService.getProfitStatisticsByTime(1).subscribe(a => {
        console.log(a);
        this.receivingProfit = a.data.receivingProfit;
        this.sendingProfit = a.data.sendingProfit;
        this.totalProfit = a.data.totalProfit;


      });
    }

    if (elem == "Monthly") {
      this.TransactionService.getProfitStatisticsByTime(3).subscribe(a => {
        console.log(a);
        this.receivingProfit = a.data.receivingProfit;
        this.sendingProfit = a.data.sendingProfit;
        this.totalProfit = a.data.totalProfit;


      });
    }



    if (elem == "Weekly") {
      this.TransactionService.getProfitStatisticsByTime(2).subscribe(a => {
        console.log(a);
        this.receivingProfit = a.data.receivingProfit;
        this.sendingProfit = a.data.sendingProfit;
        this.totalProfit = a.data.totalProfit;


      });
    }



    if (elem == "Quarterly") {
      this.TransactionService.getProfitStatisticsByTime(4).subscribe(a => {
        console.log(a);
        this.receivingProfit = a.data.receivingProfit;
        this.sendingProfit = a.data.sendingProfit;
        this.totalProfit = a.data.totalProfit;


      });
    }
  }



  getVolumeTime(elem) {


    if (elem == "Daily") {
      this.TransactionService.getTransactionStatisticsByTimeRoute(1).subscribe(a => {
        console.log(a);
        this.receivingVolume = a.data.receivingVolume;
        this.sendingVolume = a.data.sendingVolume;
        this.totalVolume = a.data.totalVolume;


      });
    }

    if (elem == "Monthly") {
      this.TransactionService.getTransactionStatisticsByTimeRoute(3).subscribe(a => {
        console.log(a);
        this.receivingVolume = a.data.receivingVolume;
        this.sendingVolume = a.data.sendingVolume;
        this.totalVolume = a.data.totalVolume;


      });
    }



    if (elem == "Weekly") {
      this.TransactionService.getTransactionStatisticsByTimeRoute(2).subscribe(a => {
        console.log(a);
        this.receivingVolume = a.data.receivingVolume;
        this.sendingVolume = a.data.sendingVolume;
        this.totalVolume = a.data.totalVolume;


      });
    }



    if (elem == "Quarterly") {
      this.TransactionService.getTransactionStatisticsByTimeRoute(4).subscribe(a => {
        console.log(a);
        this.receivingVolume = a.data.receivingVolume;
        this.sendingVolume = a.data.sendingVolume;
        this.totalVolume = a.data.totalVolume;


      });
    }


  }

  openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";



  }

  profitopenCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("profittabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("profittablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";


  }


  ngOnInit(): void {
    jQuery('.nav-tabs').on('shown.bs.tab', 'a', (e) => {
      if (e.relatedTarget) {
        jQuery(e.relatedTarget).removeClass('active');
      }
    });
  }
}

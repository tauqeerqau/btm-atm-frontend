import { Component, ViewEncapsulation,Input } from '@angular/core';
import { AppConfig } from '../app.config';
import { TransactionService } from '../services/TransactionService';
import {TransactionModel} from '../models/TransactionModel';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'opening',
  templateUrl: './opening.template.html',
  styleUrls: ['./opening.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:[TransactionService]
})
export class Opening{
  config: any;
  month: any;
  year: any;
  public transaction:TransactionModel;
  public transactionId:string;
  public block_hash:string;
  public block_height:string;
  public showTransactionDetails:boolean=false;
  public errorDiv:boolean=false;
  public block_time;confirmations;contains_dust;estimated_change;estimated_change_address;estimated_value;first_seen_at;hash;high_priority:string;
  public is_coinbase;is_double_spend;last_seen_at;lock_time_block_height;lock_time_timestamp;size;total_fee;total_input_value;total_output_value;unconfirmed_inputs:string;
  constructor(config: AppConfig,public TransactionService:TransactionService,public router: Router) {
    this.config = config.getConfig();

     
  }

  getTransactionId(){
      this.transactionId = jQuery.trim(this.transactionId);
      if(this.transactionId == undefined || this.transactionId.length==0){
         this.errorDiv = true;
      }else{
        this.errorDiv = false;

        this.TransactionService.getTransactionData(this.transactionId).subscribe(a=>{
       console.log(a);
       if(a.code==200){     

         this.transaction = a.data;
        this.block_hash = a.data.block_hash;
        this.block_height = a.data.block_height;
        this.block_time = a.data.block_time;
        this.confirmations = a.data.confirmations;
        this.contains_dust = a.data.contains_dust;
        this.estimated_change = a.data.estimated_change;
        this.estimated_change_address = a.data.estimated_change_address;
        this.estimated_value = a.data.estimated_value;
        this.first_seen_at = a.data.first_seen_at;
        this.hash = a.data.hash;
        this.high_priority = a.data.high_priority;
        this.is_coinbase = a.data.is_coinbase;
        this.is_double_spend = a.data.is_double_spend;
        this.last_seen_at = a.data.last_seen_at;
        this.lock_time_block_height = a.data.lock_time_block_height;
        this.lock_time_timestamp = a.data.lock_time_timestamp;
        this.size = a.data.size;
        this.total_fee = a.data.total_fee;
        this.total_input_value = a.data.total_input_value;
        this.total_output_value = a.data.total_output_value;
        this.unconfirmed_inputs = a.data.unconfirmed_inputs;
        this.showTransactionDetails = true;
console.log(a.data.block_time);
       }else{
         this.showTransactionDetails = false;
         jQuery('<div class="alert alert-danger wrongId">Wrong Transaction ID!</div>').insertAfter(".subscriptionBox");
         setTimeout(function() {
           jQuery(".wrongId").hide();
         }, 3000);
       }

      })



      }
     
  }
  rerouteToLogin(){
    this.router.navigate(["/login"]);
  }
  ngOnInit(): void {
    let now = new Date();
    this.month = now.getMonth() + 1;
    this.year = now.getFullYear();
  }
}

import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { ServiceUrl } from './ServiceUrl';
@Injectable()
export class LoginService {
    urlService = new ServiceUrl();
    constructor(private http: Http) {

    }
    login(userName, password) {
        let body = JSON.stringify({ userName: userName, userPassword: password });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });

        return this.http.post(this.urlService.baseUrl + "merchant/adminLogin", body, options)
            .map(res => res.json());
    }
}
import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { ServiceUrl } from './ServiceUrl';
@Injectable()
export class MerchantService {
    urlService = new ServiceUrl();
    constructor(private http: Http) {

    }
    createMerchant(name, email, userName, password) {
        let body = JSON.stringify({ userName: userName, userPassword: password, userFullName: name, userEmail: email, userRole: 2 });
        let headers = new Headers({ 'Content-Type': 'application/json', 'x-access-token':localStorage.getItem('authToken') });
        let options = new RequestOptions({ method: 'post', headers: headers });

        return this.http.post(this.urlService.baseUrl + "merchant/createMerchant", body, options)
            .map(res => res.json());
    }
    getMerchantList() {
        let body = JSON.stringify({ });
        let headers = new Headers({ 'Content-Type': 'application/json', 'x-access-token':localStorage.getItem('authToken') });
        let options = new RequestOptions({ method: 'post', headers: headers });

        return this.http.post(this.urlService.baseUrl + "merchant/getListOfMerchants", body, options)
            .map(res => res.json());
    }
    changePassword(userId, Password, OldPassword) {
        let body = JSON.stringify({ oldPassword: OldPassword, newPassword: Password, userId: userId});
        let headers = new Headers({ 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" });
        let options = new RequestOptions({ method: 'post', headers: headers });
        console.log(body);
        return this.http.post(this.urlService.baseUrl + "merchant/changePassword", body, options)
            .map(res => res.json());
    }
    deleteMerchant(merchantId) {
        let body = JSON.stringify({ '_id':merchantId });
        let headers = new Headers({ 'Content-Type': 'application/json', 'x-access-token':localStorage.getItem('authToken') });
        let options = new RequestOptions({ method: 'post', headers: headers });

        return this.http.post(this.urlService.baseUrl + "merchant/deleteMerchant", body, options)
            .map(res => res.json());
    }
}
import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { ServiceUrl } from './ServiceUrl';
@Injectable()
export class TransactionService {
    urlService = new ServiceUrl();
    constructor(private http: Http) {

    }
    getProfitStatisticsByTime(time){
        let body = JSON.stringify({ "filterTime" : time });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });
        return this.http.post(this.urlService.baseUrl + "merchant/getProfitStatisticsByTime", body, options)
            .map(res => res.json());
    }

    getTransactionStatisticsByTimeRoute(time){
        let body = JSON.stringify({ "filterTime" : time });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });
        return this.http.post(this.urlService.baseUrl + "merchant/getTransactionStatisticsByTimeRoute", body, options)
            .map(res => res.json());
    }
    getTransactionData(transactionId) {
        let body = JSON.stringify({ "transactionId" : transactionId });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });
        return this.http.post(this.urlService.baseUrl + "merchant/getTransactionData", body, options)
            .map(res => res.json());
    }

      getProfitConfiguration(merchantProfit,bitpointProfit) {
        let body = JSON.stringify({ "merchantProfit" : merchantProfit, "bitpointProfit":bitpointProfit });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });
        return this.http.post(this.urlService.baseUrl + "adminConfiguration/addProfitConfiguration", body, options)
            .map(res => res.json());
    }

    getTransactionDetailsById(merchantId){

        let body = JSON.stringify({ "merchantId":merchantId });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ method: 'post', headers: headers });
        return this.http.post(this.urlService.baseUrl + "merchant/getTransactionsByMerchantId", body, options)
            .map(res => res.json());

        
    }

}
export class User {
    _id: string;
    userEmail: string;
    userName: string;
    authenticationToken: string;
    userRole: number;
    userFullName: string;
}
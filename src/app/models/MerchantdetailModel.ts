
    export interface Datum {
        _id: string;
        transactionTime: string;
        transactionId: string;
        sendingAmount: number;
        customerAddress: string;
        merchantId: string;
        __v: number;
    }

    export interface MerchantdetailModel {
        message: string;
        code: number;
        data: Datum[];
        _id: string;
    }

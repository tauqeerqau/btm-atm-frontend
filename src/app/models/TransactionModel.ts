  export interface Output {
        spent_index: number;
        spent_hash?: any;
        script_hex: string;
        script: string;
        multisig_addresses?: any;
        multisig?: any;
        type: string;
        address: string;
        value: number;
        index: number;
    }

    export interface Input {
        script_signature: string;
        multisig_addresses?: any;
        multisig?: any;
        type: string;
        address: string;
        sequence: number;
        value: number;
        output_confirmed: boolean;
        output_index: number;
        output_hash: string;
        index: number;
    }

    export interface Data {
        double_spend_in: any[];
        is_double_spend: boolean;
        size: number;
        lock_time_block_height?: any;
        lock_time_timestamp?: any;
        unconfirmed_inputs: boolean;
        opt_in_rbf: boolean;
        outputs: Output[];
        inputs: Input[];
        contains_dust: boolean;
        enough_fee: boolean;
        high_priority: boolean;
        estimated_change_address: string;
        estimated_change: number;
        total_fee: number;
        total_output_value: number;
        total_input_value: number;
        estimated_value: number;
        is_coinbase: boolean;
        confirmations: number;
        block_hash: string;
        block_time: Date;
        block_height: number;
        last_seen_at: Date;
        first_seen_at: Date;
        hash: string;
        raw: string;
    }

    export interface TransactionModel {
        message: string;
        code: number;
        data: Data;
        _id: string;
    }
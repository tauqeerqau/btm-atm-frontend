import { Component, ViewEncapsulation } from '@angular/core';
import { MerchantService } from './../../services/MerchantService';
declare var jQuery: any;

@Component({
  selector: 'createmerchant',
  templateUrl: './create.merchant.template.html',
  styleUrls: ['./create.merchant.style.scss'],
  providers: [MerchantService],
  encapsulation: ViewEncapsulation.None
})


export class CreateMerchant {
  /**
   * Field Variables
   */
  public merchantName: String = "";
  public merchantEmail: String = "";
  public merchantPassword: String = "";
  public merchantUsername: String = "";
  /**
   * End
   */
  /**
   * Error Variables
   */
  public merchantNameErrors: String = "";
  public merchantEmailErrors: String = "";
  public merchantPasswordErrors: String = "";
  public merchantUsernameErrors: String = "";
  /**
   * End
   */
  constructor(public _merchantService: MerchantService) {

  }
  createMerchant() {
    var errorCount = 0;
    this.merchantEmailErrors = "";
    this.merchantNameErrors = "";
    this.merchantPasswordErrors = "";
    this.merchantUsernameErrors = "";
    if (this.merchantName == "" || this.merchantName === undefined) {
      this.merchantNameErrors = "Merchant Name is Required";
      errorCount += 1;
    }
    if (this.merchantEmail == "" || this.merchantEmail === undefined) {
      this.merchantEmailErrors = "Merchant Email is Required";
      errorCount += 1;
    }
    if(this.emailValidator(this.merchantEmail).invalidEmail == true){
      this.merchantEmailErrors = "Merchant Email is not Correct";
      errorCount += 1;
    }
    if (this.merchantPassword == "" || this.merchantPassword === undefined) {
      this.merchantPasswordErrors = "Merchant Password is Required";
      errorCount += 1;
    }
    if (this.merchantUsername == "" || this.merchantUsername === undefined) {
      this.merchantUsernameErrors = "Merchant Username is Required";
      errorCount += 1;
    }
    if (this.merchantUsername != "" && this.merchantUsername !== undefined && this.merchantPassword.length < 8) {
      this.merchantPasswordErrors = "Merchant Password should be of minimum 8 characters";
      errorCount += 1;
    }
    if (errorCount == 0) {
      this.merchantEmailErrors = "";
      this.merchantNameErrors = "";
      this.merchantPasswordErrors = "";
      this.merchantUsernameErrors = "";
      jQuery('.errorText').hide();
      this._merchantService.createMerchant(this.merchantName, this.merchantEmail, this.merchantUsername, this.merchantPassword).subscribe(
        response => {
          if(response.code == 200){
            this.merchantEmailErrors = "";
            this.merchantNameErrors = "";
            this.merchantPasswordErrors = "";
            this.merchantUsernameErrors = "";
            this.merchantName = "";
            this.merchantEmail = "";
            this.merchantPassword  = "";
            this.merchantUsername  = "";
          }
        }
      );
    }
    else{
      jQuery('.errorText').show();
    }
  }
  ngOnInit(): void {

  }
  generateRandom() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 8; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    console.log(text);
    this.merchantPassword = text;
  }
  emailValidator(control) {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  
    if (!EMAIL_REGEXP.test(control)) {
      return {invalidEmail: true};
    }
    else{
      return {invalidEmail: false};
    }
  }
}

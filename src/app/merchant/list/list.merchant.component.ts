import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MerchantService } from './../../services/MerchantService';
import { User } from './../../models/User';

@Component({
  selector: 'listmerchant',
  templateUrl: './list.merchant.template.html',
  styleUrls: ['./list.merchant.style.scss'],
  providers: [MerchantService],
  encapsulation: ViewEncapsulation.None
})
export class ListMerchant {

  public merchantList: User[];
  public searchList: User[];
  public Email: string;

  constructor(public _merchantService: MerchantService, public router: Router) {
    _merchantService.getMerchantList().subscribe(
      response => {
        if (response.code == 200) {
          console.log(response);
          this.merchantList = response.data;
          this.searchList = this.merchantList;
          console.log(this.merchantList);
        }
      });
  }

  deleteMerchant(merchantId) {
    this._merchantService.deleteMerchant(merchantId).subscribe(
      response => {
        if (response.code == 200) {
          for (var i = 0; i < this.merchantList.length; i++) {
            if (this.merchantList[i]._id == merchantId) {
              this.merchantList.splice(i, 1);
            }
          }
        }
      });
  }
  search() {
    var email = jQuery('#email').val();
    console.log(email);
    this.searchList = this.merchantList.filter(x=>x.userName.indexOf(email) > -1);
    console.log(this.searchList);
  }
  getTransactions(elem) {
    localStorage.setItem("merchantId", elem);
    this.router.navigate(["/app/Transactions"]);
  }
  ngOnInit(): void {

  }
}

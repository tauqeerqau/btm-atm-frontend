import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { LoginService } from './../services/LoginService';
import { User } from './../models/User';

@Component({
  selector: 'login',
  styleUrls: ['./login.style.scss'],
  templateUrl: './login.template.html',
  providers: [LoginService],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})
export class Login {
  public userName: string;
  public userPassword: string;
  public errorMessage: string;
  public userModel: User;
  public router: Router;
  public userLocal: any;
  constructor(public _loginService: LoginService, router: Router, ) {
    this.router = router;
  }
  rerouteToOpening() {
    this.router.navigate(["/opening"]);
  }
  public login() {
    this.errorMessage = "";
    jQuery('#spinner').hide();
    console.log(this.userName);
    if (this.userName !== undefined && this.userName != "" && this.userName.trim().length != 0) {
      if (this.userPassword !== undefined && this.userPassword != "" && this.userPassword.trim().length != 0) {
        jQuery('#spinner').show();
        this._loginService.login(this.userName, this.userPassword).subscribe(
          response => {
            console.log(response);
            if (response.code == 200 && response.data.userRole == 3) {
              this.userModel = response.data;
              this.userLocal = response.data._id;
              localStorage.setItem("userLocal", this.userLocal);
              localStorage.setItem("userObject", this.userModel.toString());
              localStorage.setItem("authToken", this.userModel.authenticationToken);
              this.router.navigate(["/app/frontpage"]);
            }
            else {
              jQuery('#spinner').hide();
              this.errorMessage = response.message;
              console.log(this.errorMessage);
            }
          });
      }
      else {
        this.errorMessage = "User Password cannot be empty";
      }
    }
    else {
      this.errorMessage = "User Name cannot be empty";
    }
  }
}

import { Component, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransactionService } from '../services/TransactionService';
@Component({
  selector: 'Profitconfiguration',
  styleUrls: ['./profitconfiguration.style.scss'],
  templateUrl: './profitconfiguration.template.html',
  providers: [TransactionService],
  encapsulation: ViewEncapsulation.None,
})
export class Profitconfiguration {
  public userName: string;
  public userPassword: string;
  public errorMessage: string;
  public router: Router;
  successalert: boolean = false;
  public bitpointProfit: string;
  public merchantProfit:string;
  public failurealert:boolean=false;
  constructor(public _transactionService: TransactionService, router: Router, ) {
    this.router = router;
  /*  if(localStorage.getItem("userLocal")==null){
        this.router.navigate(["/login"]);
      }else{
         
      }*/
  }

  getProfits() {
     this.successalert = false;
     this.failurealert=false;
    var error;
    if (this.bitpointProfit == undefined) {
        return false;
    }

    if (this.bitpointProfit == undefined ) {
        return false;
    }
    var addition = parseFloat(this.bitpointProfit) + parseFloat(this.merchantProfit);
    if(addition > 100 || addition<100 ){
      error = true;
      this.failurealert=true;
     
      console.log(addition);
    }else{
      this.failurealert=false;
        error = false;
    }
    if (error) {
      
    } else {
      this._transactionService.getProfitConfiguration(this.merchantProfit, this.bitpointProfit).subscribe(a => {
        console.log(a);
        if (a.code == 200) {
          this.successalert = true;
          this.bitpointProfit =undefined;
          this.merchantProfit=undefined;
           this.failurealert=false;
        } else {
          this.successalert = false;
         
        }



      })
    }

  }
  

}
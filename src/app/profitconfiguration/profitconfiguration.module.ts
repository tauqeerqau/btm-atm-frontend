import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Profitconfiguration } from './Profitconfiguration.component';

export const routes = [
  { path: '', component: Profitconfiguration, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Profitconfiguration
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export default class LoginModule {
  static routes = routes;
}
